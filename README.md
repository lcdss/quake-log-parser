# Quake Log Parser
![sd](https://img.shields.io/badge/Quake%20Log%20Parser-active-green.svg)
# Intro
It's a parser used for gather data from Quake 3 Arena log files.

## Usage
```ruby
require "quake_log_parser"

file = "path/to/the/quake/3/arena/log/file"

log_data = Quake::Log::Parser.parse(file)

puts log_data.class # Quake::Log::LogData
```

###### LogData API
- `players`
- `performance`
- `means_of_death`
- `scoreboards`
- `games`

`players` returns the players name of each game.
```ruby
{
  # ...
  2 => ["Dono da Bola", "Isgalamido", "Zeh"],
  3 => ["Dono da Bola", "Isgalamido", "Zeh", "Assasinu Credi"],
  4 => ["Zeh", "Isgalamido", "Zeh", "Assasinu Credi"],
  # ...
}
```

`performance` returns the performance of the players of each game.
```ruby
{
  # ...
  11 => {
    "Isgalamido"     => { :kills => 21, :deaths => 26, :ratio => 0.81 },
    "Dono da Bola"   => { :kills => 16, :deaths => 11, :ratio => 1.45 },
    "Zeh"            => { :kills => 14, :deaths => 14, :ratio => 1.0 },
    "Oootsimo"       => { :kills =>  5, :deaths => 22, :ratio => 0.23 },
    "Chessus"        => { :kills => 16, :deaths => 17, :ratio => 0.94 },
    "Assasinu Credi" => { :kills => 13, :deaths => 23, :ratio => 0.57 },
    "Mal"            => { :kills =>  0, :deaths =>  8, :ratio => 0.0 }
  }
  #...
}
```

`means_of_death` returns the amount of deaths by "means of death" from all games.
```ruby
{
  # ...
  6 => {
    :MOD_FALLING       =>  7,
    :MOD_TRIGGER_HURT  => 20,
    :MOD_ROCKET_SPLASH => 49,
    :MOD_ROCKET        => 29,
    :MOD_SHOTGUN       =>  7,
    :MOD_RAILGUN       =>  9,
    :MOD_MACHINEGUN    =>  9
  },
  # ...
}
```

`scoreboards` returns a complete summary of each game.
```ruby
{
  # ...
  20 => {
    :total_kills => 131,
    :players => [
      "Isgalamido",
      "Oootsimo",
      "Dono da Bola",
      "Assasinu Credi",
      "Zeh",
      "Mal"
    ],
    :kills => {
      "Isgalamido"      => { :kills => 15, :deaths => 19, :ratio => 0.79 },
      "Oootsimo"        => { :kills => 14, :deaths => 24, :ratio => 0.58 },
      "Dono da Bola"    => { :kills => 16, :deaths => 16, :ratio => 1.0 },
      "Assasinu Credi"  => { :kills => 24, :deaths => 22, :ratio => 1.09 },
      "Zeh"             => { :kills => 11, :deaths => 21, :ratio => 0.52 },
      "Mal"             => { :kills => 19, :deaths => 12, :ratio => 1.58 }
    },
    :kills_by_means => {
      :MOD_ROCKET        => 37,
      :MOD_TRIGGER_HURT  => 14,
      :MOD_RAILGUN       => 9,
      :MOD_ROCKET_SPLASH => 60,
      :MOD_MACHINEGUN    => 4,
      :MOD_SHOTGUN       => 4,
      :MOD_FALLING       => 3
    },
    :info => {
      :start_at => "6:34",
      :hostname => "Code Miner Server",
      :end_at   => "14:11"
    }
  }
  # ...
}
```

`games` store all the data gathered from the log file. [**Image**](http://i.imgur.com/0aj3Eb9.png)
```ruby
[
  # ...
  Quake::Log::Game,
  # ...
]
```

## Code Style
- follow [airbnb/ruby](https://github.com/airbnb/ruby)
