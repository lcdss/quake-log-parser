# Example script of how to use the quack log parser.
require '../lib/quake_log_parser'

file = %(#{File.absolute_path("./")}/quake_3_arena.log)

log_data = Quake::Log::Parser.parse(file)

# scoreboards = log_data.scoreboards

# players_of_each_game = log_data.players

# performance_of_each_game = log_data.performance

# means_of_death_of_each_game = log_data.means_of_death

# games = log_data.games

log_data.games.each do |g|
  puts "ID: #{g.id}"
  puts "Start time: #{g.info[:start_at]}"
  puts "End time: #{g.info[:end_at]}"
  puts "Hostname: #{g.info[:hostname]}"
  puts "Total of kills: #{g.total_kills}"
  print 'Players: '
  g.players.each do |p|
    print p.name
    print p.eql?(g.players.last) ? "\n" : ', '
  end

  puts "Performance:"
  g.players.each do |p|
    puts "  - #{p.name} (kills: #{p.performance[:kills]}, deaths: #{p.performance[:deaths]}, ratio: #{p.performance[:ratio]})"
  end

  puts 'Kills by means:'
  g.kills_by_means.keys.each do |k|
    puts "  - #{k.to_s.gsub(/_/, ' ')[4..-1].capitalize}: #{g.kills_by_means[k]}"
  end

  puts "\n"
end
