require 'test_helper'

class TestGame < Minitest::Test
  def setup
    @game = Quake::Log::Game.new(1)
  end

  def test_add_player
    assert_equal 0, @game.players.size
    player = Quake::Log::Player.new(1, "Player Name 1")
    @game.add_player(player)
    assert_equal 1, @game.players.size
    @game.add_player(player)
    assert_equal 1, @game.players.size
    player.name = "Player Name 2"
    @game.add_player(player)
    assert_equal 1, @game.players.size
    assert_equal "Player Name 2", @game.players[0].name
  end

  def test_player_is_included
    player = Quake::Log::Player.new(1, "Player Name 1")
    player2 = Quake::Log::Player.new(2, "Player Name 2")
    @game.add_player(player)
    assert_equal true, @game.player?(player)
    assert_equal false, @game.player?(player2)
  end

  def test_get_player_by_id
    player = @game.get_player_by_id(1)
    assert_nil player
    player = Quake::Log::Player.new(1, "Player Name 1")
    @game.add_player(player)
    assert_equal player, @game.get_player_by_id(1)
  end

  def test_increment_kills_when_mean_of_death_is_unknown
    @game.increment_kills(:TEST)
    assert_equal 1, @game.total_kills
    assert_equal 1, @game.kills_by_means[:MOD_UNKNOWN]
  end

  def test_increment_kills_when_mean_of_death_is_known
    @game.increment_kills(:MOD_RAILGUN)
    assert_equal 1, @game.total_kills
    assert_equal 1, @game.kills_by_means[:MOD_RAILGUN]
    @game.increment_kills(:MOD_RAILGUN)
    assert_equal 2, @game.total_kills
    assert_equal 2, @game.kills_by_means[:MOD_RAILGUN]
  end
end
