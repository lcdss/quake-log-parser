require 'test_helper'

class QuakeLogTest < Minitest::Test
  def test_if_it_has_version_world_id_and_means_of_death_constants
    assert Quake::Log::VERSION
    assert Quake::Log::WORLD_ID
    assert Quake::Log::MEANS_OF_DEATH
  end
end
