require 'test_helper'

class TestPlayer < Minitest::Test
  def setup
    @player = Quake::Log::Player.new(1, "Player Name 1")
  end

  def test_if_player_was_killed_by_world
    assert_equal true, @player.killed_by_world?(Quake::Log::WORLD_ID)
    assert_equal false, @player.killed_by_world?(2)
  end

  def test_when_player_is_killed_by_world
    @player.killed_by_world
    assert_equal 0, @player.kills
  end

  def test_when_player_is_killed_by_another_player
    player = Quake::Log::Player.new(2, "Player Name 2")
    player.killed_by(@player)
    assert_equal 1, @player.kills
    assert_equal 0, @player.deaths
    assert_equal 0, player.kills
    assert_equal 1, player.deaths
  end

  def test_when_a_player_is_killed_by_yourself
    @player.killed_by(@player)
    assert_equal 0, @player.kills
    assert_equal 1, @player.deaths
  end

  def test_player_when_is_killed_by_world
    @player.killed_by_world
    assert_equal 0, @player.kills
    @player.kills = 2
    @player.killed_by_world
    assert_equal 1, @player.kills
  end

  def test_ratio
    assert_equal 0, @player.ratio
    @player.kills = 5
    assert_equal 5, @player.ratio
    @player.deaths = 5
    assert_equal 1, @player.ratio
    @player.kills = 0
    assert_equal 0, @player.ratio
  end
end
