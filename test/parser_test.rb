require 'test_helper'

class TestParser < Minitest::Test
  def setup
    @parser = Quake::Log::Parser
    @fixtures_path = "#{File.dirname(__FILE__)}/fixtures"
  end

  def test_when_file_does_not_exist_or_is_not_readable_or_is_not_a_quake_log
    assert_equal [], @parser.parse('this/file/does/not/exist')
    file = File.absolute_path(__FILE__)
    assert_equal true, File.exist?(file)
    assert_equal [], @parser.parse(file).games
  end

  def test_if_the_game_was_initialized
    content = IO.read("#{@fixtures_path}/init_game")
    assert_equal true, @parser.init_game?(content)
    content = IO.read("#{@fixtures_path}/kill")
    assert_equal false, @parser.init_game?(content)
  end

  def test_extract_game_info
    content = IO.read("#{@fixtures_path}/init_game")
    info = @parser.game_info(content)
    assert_equal "20:37", info[:start_at]
    assert_equal "Code Miner Server", info[:hostname]
  end

  def test_if_has_kill_info
    content = IO.read("#{@fixtures_path}/kill")
    assert_equal true, @parser.kill?(content)
    content = IO.read("#{@fixtures_path}/init_game")
    assert_equal false, @parser.kill?(content)
  end

  def test_extract_kill_info
    content = IO.read("#{@fixtures_path}/kill")
    info = @parser.kill_info(content)
    assert_equal "3", info[:killer_id]
    assert_equal "2", info[:victim_id]
    assert_equal "MOD_ROCKET", info[:mean_of_death]
  end

  def test_if_player_info_has_changed
    content = IO.read("#{@fixtures_path}/player_info")
    assert_equal true, @parser.player_info?(content)
  end

  def test_extract_player_info
    content = IO.read("#{@fixtures_path}/player_info")
    info = @parser.player_info(content)
    assert_equal "2", info[:player_id]
    assert_equal "Isgalamido", info[:player_name]
  end

  def test_if_the_game_ended
    content = IO.read("#{@fixtures_path}/shutdown_game")
    assert_equal true, @parser.shutdown_game?(content)
    content = IO.read("#{@fixtures_path}/kill")
    assert_equal false, @parser.shutdown_game?(content)
  end

  def test_extract_shutdown_info
    content = IO.read("#{@fixtures_path}/shutdown_game")
    info = @parser.shutdown_game_info(content)
    assert_equal "1:47", info[:end_at]
  end
end
